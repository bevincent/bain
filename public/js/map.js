var outdoors = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 16,
    attribution: '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    id: 'mapbox/outdoors-v11', 
    tileSize: 512,
    zoomOffset: -1
})

var satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    maxZoom: 19,
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});

var baseLayers = {
    "outdoors": outdoors,
    "satellite": satellite,
};

var mymap = L.map('map',{
    fullscreenControl: true,
    center: [44.715, 5.48],
    zoom: 14,
    layers: [outdoors]
  });

var Circuit1Path = [
"GPSTracks/1_Reviron_Tricot.gpx",
"GPSTracks/2_Tricot_Gue.gpx",
"GPSTracks/3_Gue_Baume.gpx",
"GPSTracks/4_Baume_Caux.gpx",
"GPSTracks/5_Caux_Bain.gpx",
"GPSTracks/6_Bain_Ferme.gpx",
"GPSTracks/7_Ferme_WP1.gpx",
"GPSTracks/8_WP1_Gue2.gpx",
"GPSTracks/9_Gue2_Champs.gpx",
"GPSTracks/10_Champs_Gue.gpx"
];

var Circuit2Path = [
"GPSTracks/16_Reviron_Rossignol.gpx",
"GPSTracks/17_Rossignol_Gorodel.gpx",
"GPSTracks/18_Gorodel_Bicha.gpx",
"GPSTracks/19_Bicha_Ronde.gpx",
"GPSTracks/20_Bicha_Long.gpx",
"GPSTracks/21_Long_OursiereHaut.gpx",
"GPSTracks/41_OursiereMilieu_OursiereHaut.gpx",
"GPSTracks/39_WP5_OursiereMilieu.gpx",
"GPSTracks/37_WP4_WP5.gpx",
"GPSTracks/36_Suel_WP4.gpx",
"GPSTracks/22_Tricot_Suel.gpx",
"GPSTracks/1_Reviron_Tricot.gpx"
];

var Circuit3Path = [
"GPSTracks/1_Reviron_Tricot.gpx",
"GPSTracks/2_Tricot_Gue.gpx",
"GPSTracks/8_WP1_Gue2.gpx",
"GPSTracks/9_Gue2_Champs.gpx",
"GPSTracks/10_Champs_Gue.gpx",
"GPSTracks/12_Trintrin_Roure.gpx",
"GPSTracks/13_WP1_WP2.gpx",
"GPSTracks/15_WP2_Trintrin.gpx",
"GPSTracks/22_Tricot_Suel.gpx",
"GPSTracks/24_Suel_SuelHaut.gpx",
"GPSTracks/25_SuelHaut_WP3.gpx",
"GPSTracks/26_WP3_OursiereBas.gpx",
"GPSTracks/29_Roure_Grottes.gpx",
"GPSTracks/31_Grotte_GrangesBas.gpx",
"GPSTracks/42_GrangesBas_KernGR.gpx",
"GPSTracks/44_KernGR_BrebisHaut.gpx",
"GPSTracks/45_BrebisHaut_BrebisBas.gpx",
"GPSTracks/46_BrebisBas_Francine.gpx",
"GPSTracks/47_Francine_OursiereBas.gpx",
];

var Circuit4Path = [
"GPSTracks/1_Reviron_Tricot.gpx",
"GPSTracks/2_Tricot_Gue.gpx",
"GPSTracks/3_Gue_Baume.gpx",
"GPSTracks/4_Baume_Caux.gpx",
"GPSTracks/5_Caux_Bain.gpx",
"GPSTracks/11_Bain_Trintrin.gpx",
"GPSTracks/12_Trintrin_Roure.gpx",
"GPSTracks/29_Roure_Grottes.gpx",
"GPSTracks/31_Grotte_GrangesBas.gpx",
"GPSTracks/50_GrangesBas_Granges.gpx",
"GPSTracks/51_Granges_Long.gpx",
"GPSTracks/16_Reviron_Rossignol.gpx",
"GPSTracks/17_Rossignol_Gorodel.gpx",
"GPSTracks/18_Gorodel_Bicha.gpx",
"GPSTracks/20_Bicha_Long.gpx"
];

function display_gpx(map,listOfFilesPath,Path,color) {
  for (var i = 0; i < listOfFilesPath.length; i += 1) {
  new L.GPX(listOfFilesPath[i], {
    async: true,
    marker_options: {
    startIconUrl: '',
    endIconUrl: '',
    shadowUrl: ''
    },
    polyline_options: {
      color: color,
      opacity: 0.75,
      weight: 3,
      lineCap: 'round'},
    async: true }
    ).on('loaded', function(e) {
      map.fitBounds(e.target.getBounds());
    }).addTo(Path);
  }
}

var Circuit1 = L.layerGroup();
display_gpx(map,Circuit1Path,Circuit1,'blue');

var Circuit2 = L.layerGroup();
display_gpx(map,Circuit2Path,Circuit2,'red');

var Circuit3 = L.layerGroup();
display_gpx(map,Circuit3Path,Circuit3,'green');

var Circuit4 = L.layerGroup();
display_gpx(map,Circuit4Path,Circuit4,'orange');

var overlays = {
  "Pié de Boeuf": Circuit1,
  "Tour de Marateste": Circuit2,
  "Ruissaux de Baïn": Circuit3,
  "Ronde de Baïn": Circuit4
};

L.control.scale().addTo(mymap);
L.control.layers(baseLayers,overlays).addTo(mymap);